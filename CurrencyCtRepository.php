<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 4:29
 */

namespace App\Repositories;


use App\Models\CurrencyCt;

class CurrencyCtRepository
{
    const GENERATE_ADDRESS_INACTIVE = 0;
    const GENERATE_ADDRESS_ACTIVE = 1;

    private $currencyCt;

    public function __construct(CurrencyCt $currencyCt)
    {
        $this->currencyCt = $currencyCt;
    }

    public function create($attributes)
    {
        return $this->currencyCt->create($attributes);
    }

    public function all()
    {
        return $this->currencyCt->all();
    }

    public function find($id)
    {
        return $this->currencyCt->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->currencyCt->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->currencyCt->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->currencyCt->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchAttributes, $setAttributes)
    {
        return $this->currencyCt->updateOrCreate($searchAttributes, $setAttributes);
    }

    public function where($condition)
    {
        return $this->currencyCt->where($condition);
    }
}