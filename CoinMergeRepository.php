<?php


namespace App\Repositories;


use App\Models\CoinMerge;

class CoinMergeRepository
{
    const STATUS_WAIT = 'W';
    const STATUS_ING = 'I';
    const STATUS_SUCCESS = 'S';
    const STATUS_FAIL = 'F';
    const STATUS_VERIFICATE = 'V';

    private $coinMerge;

    public function __construct(CoinMerge $coinMerge)
    {
        $this->coinMerge = $coinMerge;
    }

    public function create($attributes)
    {
        return $this->coinMerge->create($attributes);
    }

    public function all()
    {
        return $this->coinMerge->all();
    }

    public function find($id)
    {
        return $this->coinMerge->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->coinMerge->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->coinMerge->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->coinMerge->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchParams, $attributes)
    {
        return $this->coinMerge->updateOrCreate($searchParams, $attributes);
    }

    public function where($attr)
    {
        return $this->coinMerge->where($attr);
    }

    public function whereIn($column, $values)
    {
        return $this->coinMerge->whereIn($column, $values);
    }
}