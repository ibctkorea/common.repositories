<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 4:29
 */

namespace App\Repositories;


use App\Models\CurrencyInfo;

class CurrencyInfoChartRepository
{
    private $currencyInfoChart;

    public function __construct(CurrencyInfoChart $currencyInfoChart)
    {
        $this->currencyInfoChart = $currencyInfoChart;
    }

    public function all()
    {
        return $this->currencyInfoChart->all();
    }


}
