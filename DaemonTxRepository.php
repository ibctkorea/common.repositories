<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 4:29
 */

namespace App\Repositories;


use App\Models\DaemonTx;
use Illuminate\Support\Facades\DB;

class DaemonTxRepository
{
    private $daemonTx;

    const TYPE_DEPOSIT = 'deposit';
    const TYPE_WITHDRAW = 'withdraw';
    const TYPE_MERGE = 'withdraw';
    const TYPE_FEE = 'withdraw';
    const TYPE_DEPOSIT_INTERNAL = 'deposit_internal';
    const TYPE_WITHDRAW_INTERNAL = 'withdraw_internal';

    const STATUS_WAIT = 'W';
    const STATUS_ING = 'I';
    const STATUS_SUCCESS = 'S';
    const STATUS_FAIL = 'F';

    // W-대기중 / I-진행중 / S-완료
    const STATUS_DEPOSIT_SUCCESS = 'S';
    const STATUS_DEPOSIT_WAIT = 'W';
    const STATUS_DEPOSIT_ING = 'I';

    public function __construct(DaemonTx $daemonTx)
    {
        $this->daemonTx = $daemonTx;
    }

    public function create($attributes)
    {
        return $this->daemonTx->create($attributes);
    }

    public function all()
    {
        return $this->daemonTx->all();
    }

    public function find($id)
    {
        return $this->daemonTx->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->daemonTx->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->daemonTx->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->daemonTx->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchParams, $attributes)
    {
        return $this->daemonTx->updateOrCreate($searchParams, $attributes);
    }

    public function where($attr)
    {
        return $this->daemonTx->where($attr);
    }

    public function whereIn($column, $values)
    {
        return $this->daemonTx->whereIn($column, $values);
    }

    public function getWithdrawFeeAndGasFee($startTime,$endTime)
    {
        $query = DB::table(DB::raw("tpurcow_daemon_tx as dtx"))
            ->join(DB::raw("tpurcow_currency_info as currency"), DB::raw('dtx.currency_id'), '=', DB::raw('currency.id'))
            ->select(DB::raw("DATE_FORMAT(dtx.updated_at, '%Y-%m-%d') as date ,type, sum(fee) as fee, currency_id, symbol"))
            ->whereIn('type', [DaemonTxRepository::TYPE_WITHDRAW, 'merge']) //merge 는 나중에 wallet작업끝난 후 const로 변경 예정
            ->where('status', '=', DaemonTxRepository::STATUS_SUCCESS)
            ->whereBetween(DB::raw("dtx.updated_at"), [$startTime, $endTime])//Carbon::today()]) //today 오늘 00:00:00 까지
            ->groupBy(DB::raw("DATE_FORMAT(dtx.updated_at, '%Y-%m-%d'), type, dtx.currency_id"));
        return $query->get();
    }
}
