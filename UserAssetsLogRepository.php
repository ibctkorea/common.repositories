<?php


namespace App\Repositories;


use App\Models\UserAssetsLog;

class UserAssetsLogRepository
{
    const TYPE_WITHDRAW = 'withdraw';
    const TYPE_DEPOSIT = 'deposit';
    const TYPE_SELL = 'sell';
    const TYPE_BUY = 'buy';

    /**
     * @var UserAssetsLog
     */
    private $userAssetsLog;

    public function __construct(UserAssetsLog $userAssetsLog)
    {
        $this->userAssetsLog = $userAssetsLog;
    }

    public function create($attributes)
    {
        return $this->userAssetsLog->create($attributes);
    }

    public function all()
    {
        return $this->userAssetsLog->all();
    }

    public function find($id)
    {
        return $this->userAssetsLog->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->userAssetsLog->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->userAssetsLog->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->userAssetsLog->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchParams, $attributes)
    {
        return $this->userAssetsLog->updateOrCreate($searchParams, $attributes);
    }

    public function where($attr)
    {
        return $this->userAssetsLog->where($attr);
    }

    public function whereIn($column, $values)
    {
        return $this->userAssetsLog->whereIn($column, $values);
    }
}