<?php


namespace App\Repositories;


use App\Models\Member;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MemberRepository
{
    const USER_PROVE_NOT = 0;
    const USER_PROVE_REQUEST = 1;
    const USER_PROVE_APPROVAL = 2;
    const USER_PROVE_REJECT = 3;
    const MEMBER_ACTIVE = 1;
    const MEMBER_INACTIVE = 2;

    const IS_NOT_ROBOT = 0;


    /**
     * @var Member
     */
    private $member;

    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    public function create($attributes)
    {
        return $this->member->create($attributes);
    }

    public function all()
    {
        return $this->member->all();
    }

    public function find($id)
    {
        return $this->member->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->member->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->member->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->member->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchParams, $attributes)
    {
        return $this->member->updateOrCreate($searchParams, $attributes);
    }

    public function where($attr)
    {
        return $this->member->where($attr);
    }

    public function whereIn($column, $values)
    {
        return $this->member->whereIn($column, $values);
    }

    public function updateDb($id, $attributes)
    {
        return DB::table('member')->where('member_id', $id)->update($attributes);
    }
    public function getUpdateCheck($attributes)
    {
        return DB::table('member')->where($attributes)->select('member_id')->first();
    }

    public function detail($memberId)
    {
        $query = DB::table('member')
            ->leftJoin('mem_bank', function ($join) {
                $join->on('member.member_id', '=', 'mem_bank.member_id')
                    ->where('mem_bank.isdel', 0);
            })
            ->leftJoin('bank_info', 'bank_info.bank_code', '=', 'mem_bank.bank_code');

        $query->select('member.member_id as member_id', 'name', 'phone', 'email', 'ga_open', 'user_prove', 'bank_accnt', 'bank_name', 'member.status', 'login_time', 'reg_time', 'member.ip');
        $query->where('member.member_id', $memberId);

        return $query->first();
    }

    public function paginate($perPage, $condition, $orderBy = [])
    {
        $query = DB::table('member')
            ->leftJoin('member as member2', 'member.pid', '=', 'member2.invitation');

        if (!empty($condition['search_coin'])) {
            $query->leftJoin('currency_user', 'currency_user.member_id', '=', 'member.member_id')
                ->where('currency_user.currency_id', $condition['search_coin'])
                ->where('currency_user.num', '>', 0);
        }

        $query->leftJoin('mem_bank as mb', function ($join) {
            $join->on('member.member_id', '=', 'mb.member_id')
                ->where('mb.isdel', 0);
        });

        $query->select('member.member_id', 'member.phone', 'member.email', 'member.name', 'member2.member_id as pid_id', 'mb.bank_accnt', 'member.invitation',
            'member.ga_open', 'member.user_prove', 'member.status', 'member.reg_time')
            ->where('member.is_robot', 0);


        if (!empty($condition['member_id'])) {
            if (gettype($condition['member_id']) == 'array') {  //배열안에 값이 있을 때
                $query->whereIn('member.member_id', $condition['member_id']);
            } else {
                $query->where('member.member_id', $condition['member_id']);
            }
        }
        if (gettype($condition['member_id']) == 'array') { //배열안에 값이 없을 때
            $query->whereIn('member.member_id', $condition['member_id']);
        }


        if (!empty($condition['email'])) {
            $query->where('member.email', 'like', '%'.$condition['email'].'%');
        }
        if (!empty($condition['phone'])) {
            $query->where('member.phone', 'like', '%'.$condition['phone'].'%');
        }
        if (!empty($condition['name'])) {
            $query->where('member.name', 'like', '%'.$condition['name'].'%');
        }
        if (!empty($condition['search_date_start']) && !empty($condition['search_date_end'])) {
            $query->where('member.reg_time', '>=', Carbon::make($condition['search_date_start'])->timestamp)
                ->where('member.reg_time', '<=', Carbon::make($condition['search_date_end'])->setTime(23, 59, 59)->timestamp);
        }

        if (!empty($condition['search_status'])) {
            if ($condition['search_status'] == 'inactive') {
                $query->where('member.status', '=', 2); // 2 정지계정
            } else { //활동 회원
                $query->where('member.status', '!=', 2); // 2 정지계정
            }
        }
        if (!empty($condition['search_level'])) {

            switch ($condition['search_level']) {
                case 'Lv.0':
                    $query->where('member.phone', null)
                        ->orWhere('member.email', null);
                    break;
                case 'Lv.1':
                    $query->where('member.phone', '!=', null)
                        ->where('member.email', '!=', null)
                        ->where('member.ga_open', 0);
                    break;
                case 'Lv.2':
                    $query->where('member.phone', '!=', null)
                        ->where('member.email', '!=', null)
                        ->where('member.ga_open', 1)
                        ->where('member.user_prove', '!=', 2);
                    break;
                case 'Lv.3' :
                    $query->where('member.phone', '!=', null)
                        ->where('member.email', '!=', null)
                        ->where('member.ga_open', 1)
                        ->where('member.user_prove', 2)
                        ->where('mb.bank_accnt', null);
                    break;
                case 'Lv.4':
                    $query->where('member.phone', '!=', null)
                        ->where('member.email', '!=', null)
                        ->where('member.ga_open', 1)
                        ->where('member.user_prove', 2)
                        ->where('mb.bank_accnt', '!=', null);
                    break;
            }
        }

        if ($orderBy['column'] == null || $orderBy['sort'] == null) {
            $query->orderBy('member_id', 'desc');
        } else {
            $query->orderBy($orderBy['column'], $orderBy['sort']);
        }
//        $query->orderBy('symbol', 'desc');
        return $query->paginate($perPage);
    }


    public function getOrderCountMonthly()
    {
        $query = DB::table('order_book')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%Y-%m')"))
            ->select(DB::raw("count(id) as cnt, DATE_FORMAT(created_at, '%Y-%m') as month"))
            ->orderBy('month', 'desc')
            ->limit(12);
        return $query->get();
    }

    public function getTransactionCountMonthly()
    {
        $query = DB::table('coin_transaction')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%Y-%m')"))
            ->select(DB::raw("count(id) as cnt, DATE_FORMAT(created_at, '%Y-%m') as month"))
            ->orderBy('month', 'desc')
            ->limit(12);
        return $query->get();
    }


    public function getLoginMonthly()
    {
        $query = DB::table('member')
            ->groupBy(DB::raw("DATE_FORMAT(FROM_UNIXTIME(reg_time), '%y-%m')"))
            ->select(DB::raw("count(member_id) as cnt, DATE_FORMAT(FROM_UNIXTIME(reg_time), '%y-%m') as value"))
            ->orderBy('value', 'desc')
            ->limit(12)
            ->whereNotNull('reg_time');
        return $query->get();
    }


    public function getLoginDaily()
    {
        $query = DB::table('member')
            ->groupBy(DB::raw("DATE_FORMAT(FROM_UNIXTIME(reg_time), '%y-%m-%d')"))
            ->select(DB::raw("count(member_id) as cnt, DATE_FORMAT(FROM_UNIXTIME(reg_time), '%y-%m-%d') as value"))
            ->orderBy('value', 'desc')
            ->limit(20)
            ->whereNotNull('reg_time');
        return $query->get();

    }


}
