<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 4:29
 */

namespace App\Repositories;



use App\Models\Tibi;

class TibiRepository
{
    private $tibi;

    public function __construct(Tibi $tibi)
    {
        $this->tibi = $tibi;
    }

    public function create($attributes)
    {
        return $this->tibi->create($attributes);
    }

    public function all()
    {
        return $this->tibi->all();
    }

    public function find($id)
    {
        return $this->tibi->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->tibi->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->tibi->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->tibi->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchParams, $attributes)
    {
        return $this->tibi->updateOrCreate($searchParams, $attributes);
    }

    public function where($attr)
    {
        return $this->tibi->where($attr);
    }

    public function whereIn($column, $values)
    {
        return $this->tibi->whereIn($column, $values);
    }
}