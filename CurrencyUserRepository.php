<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 9. 27
 * Time: 오전 10:39
 */

namespace App\Repositories;


use App\Models\CurrencyUser;
use Illuminate\Support\Facades\DB;

class CurrencyUserRepository
{
    private $currencyUser;

    public function __construct(CurrencyUser $currencyUser)
    {
        $this->currencyUser = $currencyUser;
    }

    public function create($attributes)
    {
        return $this->currencyUser->create($attributes);
    }

    public function all()
    {
        return $this->currencyUser->all();
    }

    public function find($id)
    {
        return $this->currencyUser->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->currencyUser->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->currencyUser->find($id)->delete();
    }

    public function where($condition)
    {
        return $this->currencyUser->where($condition);
    }

    public function whereIn($column, $arr)
    {
        return $this->currencyUser->whereIn($column, $arr);
    }

    public function firstOrCreate($attributes)
    {
        return $this->currencyUser->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchAttributes, $setAttributes)
    {
        return $this->currencyUser->updateOrCreate($searchAttributes, $setAttributes);
    }

    /**
     * @param $currencyId
     * @param $memberId
     * @return CurrencyUser
     */
    public function createCurrencyUser($currencyId, $memberId): CurrencyUser
    {
        $currencyUser = new CurrencyUser;
        $currencyUser->member_id = $memberId;
        $currencyUser->currency_id = $currencyId;
        $currencyUser->num = 0;
        $currencyUser->forzen_num = 0;
        $currencyUser->block_type = 1;
        $currencyUser->save();
        return $currencyUser;
    }

    public function getAllAssets($array)
    {
        $query = DB::table('currency_user')
            ->groupBy('currency_id')
            ->join('currency_info', 'currency_user.currency_id', '=', 'currency_info.id')
            ->select(DB::raw('sum(num) as amount, currency_id, symbol'))
            ->whereIn('currency_user.currency_id', $array);
        return $query->get();

    }




}
