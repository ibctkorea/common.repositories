<?php


namespace App\Repositories;


use App\Models\Orders;

class OrdersRepository
{
    private $orders;

    public function __construct(Orders $orders)
    {
        $this->orders = $orders;
    }

    public function find($id)
    {
        return $this->orders->find($id);
    }

    public function where($condition,$value)
    {
        return $this->orders->where($condition,$value);
    }



    public function whereIn($column, $values)
    {
        return $this->orders->whereIn($column, $values);
    }
    public function all()
    {
        return $this->orders->all();
    }


}
