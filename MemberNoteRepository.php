<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 7. 4
 * Time: 오전 9:24
 */

namespace App\Repositories;


use App\Models\MemberNote;
use Illuminate\Support\Facades\DB;


class MemberNoteRepository
{

    /**
     * @var MemberNote
     */
    private $memberNote;

    public function __construct(MemberNote $memberNote)
    {
        $this->memberNote = $memberNote;
    }

    public function create($attributes)
    {
        return $this->memberNote->create($attributes);
    }

    public function all()
    {
        return $this->memberNote->all();
    }

    public function find($id)
    {
        return $this->memberNote->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->memberNote->find($id)->update($attributes);
    }

    public function findLock($id)
    {
        return $this->memberNote->where('id', $id)->lockForUpdate()->first();
    }

    public function whereLock($condition)
    {
        return $this->memberNote->where($condition)->lockForUpdate()->get();
    }

    public function where($condition)
    {
        return $this->memberNote->where($condition);
    }
    public function orWhere($condition)
    {
        return $this->memberNote->orWhere($condition);
    }
    public function updateOrCreate($searchParams, $attributes)
    {
        return $this->memberNote->updateOrCreate($searchParams, $attributes);
    }


}
