<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 4:29
 */

namespace App\Repositories;


use App\Models\Currency;

class CurrencyRepository
{
    const STATUS_DEPOSIT_INACTIVE = 0;
    const STATUS_DEPOSIT_ACTIVE = 1;

    private $currency;

    public function __construct(Currency $currency)
    {
        $this->currency = $currency;
    }

    public function create($attributes)
    {
        return $this->currency->create($attributes);
    }

    public function all()
    {
        return $this->currency->all();
    }

    public function find($id)
    {
        return $this->currency->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->currency->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->currency->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->currency->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchAttributes, $setAttributes)
    {
        return $this->currency->updateOrCreate($searchAttributes, $setAttributes);
    }

    public function findBySymbol($symbol)
    {
        return $this->currency->where("currency_mark", $symbol)->first();
    }
}