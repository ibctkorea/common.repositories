<?php

namespace App\Repositories;


use App\Models\Test;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class TestRepository
{
    private $test;

    public function __construct(Test $test)
    {
        $this->test = $test;
    }

    public function create($attributes)
    {
        return $this->test->create($attributes);
    }

    public function all()
    {
        return $this->test->all();
    }

    public function find($id)
    {
        return $this->test->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->test->find($id)->update($attributes);
    }

    public function updateWhere($searchArr, $attributes)
    {
        return $this->test->where($searchArr)->update($attributes);
    }

    public function updateOrCreate($condition, $values)
    {
        return $this->test->updateOrCreate($condition, $values);
    }

    public function findLock($id)
    {
        return $this->test->where('id', $id)->lockForUpdate()->first();
    }

    public function whereLock($condition)
    {
        return $this->test->where($condition)->lockForUpdate()->get();
    }

    public function where($condition)
    {
        return $this->test->where($condition);
    }

    public function whereIn($column, $values)
    {
        return $this->test->whereIn($column, $values);
    }

    public function orWhere($condition)
    {
        return $this->test->orWhere($condition);
    }

    public function insert($arr)
    {
        return $this->test->insert($arr);
    }

    public function getStatisticsMonth($startTime, $endTime)
    {

        $query = DB::table('statistics')
            ->groupBy(DB::raw("DATE_FORMAT(created_at,'%y-%m'),currency_id,symbol"))
            ->havingBetween('date', [$startTime, $endTime])
            ->select(DB::raw("currency_id,symbol,DATE_FORMAT(created_at,'%y-%m') as date,sum(withdraw_fee) as withdraw_fee,
            sum(merge_fee) as merge_fee ,sum(gas_fee) as gas_fee, sum(trade_fee) as trade_fee, sum(deposit_amount) as deposit_amount, 
            sum(withdraw_amount) as withdraw_amount, sum(trade_amount) as trade_amount, sum(withdraw_fee)+sum(trade_fee)  as total_revenue"))
            ->orderBy(DB::raw("date, field(currency_id,189) DESC, currency_id"));
        return $query->get();
    }

    public function getStatisticsDay($time)
    {
        $query = DB::table('statistics')
            ->select(DB::raw("currency_id,symbol,created_at as date,withdraw_fee,merge_fee,gas_fee,trade_fee,
            deposit_amount,withdraw_amount,trade_amount,withdraw_fee+trade_fee as total_revenue"))
            ->whereBetween('created_at', [$time, $time])
            ->orderBy(DB::raw("field(currency_id,189) DESC, currency_id"));
        return $query->get();
    }

    public function getStatisticsTotal()
    {
        $query = DB::table('statistics')
            ->groupBy(DB::raw("currency_id,symbol"))
            ->select(DB::raw("currency_id, sum(deposit_amount) as deposit_amount, sum(withdraw_amount) as withdraw_amount, sum(withdraw_fee) as withdraw_fee,
            sum(trade_fee) as trade_fee"));
        return $query->get();
    }

    public function test()
    {
        $query = "
            select * from (select currency_id,symbol,sum(deposit_cnt) as total_deposit_cnt,sum(deposit_amount) as total_deposit_amount,
            sum(withdraw_cnt) as total_withdraw_cnt,sum(withdraw_amount) as total_withdraw_amount,sum(trade_amount) as total_trade_amount,
            sum(trade_amount_krw) as total_trade_amount_krw ,(sum(trade_fee)) as total_trade_fee,sum(merge_fee) as total_merge_fee, sum(gas_fee) as total_gas_fee
            from tpurcow_statistics group by currency_id,symbol) as statistics
            left join (select currency_id,sum(num) as total_amount from tpurcow_currency_user group by currency_id) as currencyUser on currencyUser.currency_id = statistics.currency_id
            left join (select currency_id,sum(amount) as used_assets from tpurcow_used_assets where deleted_at is null group by currency_id) as usedAsstes on usedAsstes.currency_id = statistics.currency_id
            left join (
                select target.currency_id as currency_id, buy_fee,sell_fee, (ifnull(buy_fee,0)+ifnull(sell_fee,0)) as trade_fee from (select target_currency_id as currency_id,sum(buy_fee) as buy_fee from tpurcow_trade_contract  where created_at>'2020-03-11' group by target_currency_id) as target
                left join (select base_currency_id as currency_id,sum(sell_fee) as sell_fee from tpurcow_trade_contract  where created_at>'2020-03-11' group by base_currency_id )as base  on target.currency_id = base.currency_id
                union
                select base.currency_id as currency_id, buy_fee,sell_fee, (ifnull(buy_fee,0)+ifnull(sell_fee,0)) as trade_fee from (select base_currency_id as currency_id,sum(sell_fee) as sell_fee from tpurcow_trade_contract  where created_at>'2020-03-11' group by base_currency_id )as base
                left join  (select target_currency_id as currency_id,sum(buy_fee) as buy_fee from tpurcow_trade_contract  where created_at>'2020-03-11' group by target_currency_id) as target  on target.currency_id = base.currency_id
            ) as trade on trade.currency_id = statistics.currency_id";

        return DB::select($query);
    }

}
