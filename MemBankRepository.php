<?php

namespace App\Repositories;


use App\Models\MemBank;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MemBankRepository
{
    /**
     * @var MemBank
     */
    private $memBank;

    public function __construct(MemBank $memBank)
    {
        $this->memBank = $memBank;
    }

    public function create($attributes)
    {
        return $this->memBank->create($attributes);
    }

    public function all()
    {
        return $this->memBank->all();
    }

    public function find($id)
    {
        return $this->memBank->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->memBank->find($id)->update($attributes);
    }

    public function updateDb($id, $attributes)
    {
        return DB::table('mem_bank')->where('member_id', $id)->update($attributes);
    }

    public function findLock($id)
    {
        return $this->memBank->where('id', $id)->lockForUpdate()->first();
    }

    public function whereLock($condition)
    {
        return $this->memBank->where($condition)->lockForUpdate()->get();
    }

    public function where($condition)
    {
        return $this->memBank->where($condition);
    }

    public function whereIn($column, $values)
    {
        return $this->memBank->whereIn($column, $values);
    }

    public function orWhere($condition)
    {
        return $this->memBank->orWhere($condition);
    }


    public function whereOrderBy($condition, $orderBy, $limit = 10)
    {
        $query = $this->memBank->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'desc');
        }

        return $query->limit($limit)->get();
    }

    public function whereOrderByOne($condition, $orderBy)
    {
        $query = $this->memBank->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'desc');
        }

        return $query->first();
    }


}
