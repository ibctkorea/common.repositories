<?php

namespace App\Repositories;


use App\Models\Statistics;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class StatisticsRepository
{
    /**
     * @var statistics
     */
    private $statistics;

    public function __construct(Statistics $statistics)
    {
        $this->statistics = $statistics;
    }

    public function create($attributes)
    {
        return $this->statistics->create($attributes);
    }

    public function all()
    {
        return $this->statistics->all();
    }

    public function find($id)
    {
        return $this->statistics->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->statistics->find($id)->update($attributes);
    }

    public function updateWhere($searchArr, $attributes)
    {
        return $this->statistics->where($searchArr)->update($attributes);
    }

    public function updateOrCreate($condition, $values)
    {
        return $this->statistics->updateOrCreate($condition, $values);
    }

    public function findLock($id)
    {
        return $this->statistics->where('id', $id)->lockForUpdate()->first();
    }

    public function whereLock($condition)
    {
        return $this->statistics->where($condition)->lockForUpdate()->get();
    }

    public function where($condition)
    {
        return $this->statistics->where($condition);
    }

    public function whereIn($column, $values)
    {
        return $this->statistics->whereIn($column, $values);
    }

    public function orWhere($condition)
    {
        return $this->statistics->orWhere($condition);
    }

    public function insert($arr)
    {
        return $this->statistics->insert($arr);
    }

    public function getStatisticsMonth($startTime, $endTime)
    {

        $query = DB::table('statistics')
            ->groupBy(DB::raw("DATE_FORMAT(created_at,'%y-%m'),currency_id,symbol"))
            ->havingBetween('date', [$startTime, $endTime])
            ->select(DB::raw("currency_id,symbol,DATE_FORMAT(created_at,'%y-%m') as date,sum(withdraw_fee) as withdraw_fee,
            sum(merge_fee) as merge_fee ,sum(gas_fee) as gas_fee, sum(trade_fee) as trade_fee, sum(deposit_amount) as deposit_amount, 
            sum(withdraw_amount) as withdraw_amount, sum(trade_amount) as trade_amount, sum(withdraw_fee)+sum(trade_fee)  as total_revenue"))
            ->orderBy(DB::raw("date, field(currency_id,189) DESC, currency_id"));
        return $query->get();
    }

    public function getStatisticsDay($time)
    {
        $query = DB::table('statistics')
            ->select(DB::raw("currency_id,symbol,created_at as date,withdraw_fee,merge_fee,gas_fee,trade_fee,
            deposit_amount,withdraw_amount,trade_amount,withdraw_fee+trade_fee as total_revenue"))
            ->whereBetween('created_at', [$time, $time])
            ->orderBy(DB::raw("field(currency_id,189) DESC, currency_id"));
        return $query->get();
    }

    public function getStatisticsTotal()
    {
        $query = DB::table('statistics')
            ->groupBy(DB::raw("currency_id,symbol"))
            ->select(DB::raw("currency_id, sum(deposit_amount) as deposit_amount, sum(withdraw_amount) as withdraw_amount, sum(withdraw_fee) as withdraw_fee,
            sum(trade_fee) as trade_fee"));
        return $query->get();
    }

}
