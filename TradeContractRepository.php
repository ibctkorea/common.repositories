<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 7. 4
 * Time: 오전 9:24
 */

namespace App\Repositories;


use App\Models\TradeContract;
use Illuminate\Support\Facades\DB;

class TradeContractRepository
{
    const STATUS_WAIT = 0;          // 대기
    const STATUS_COMPLETE = 1;      // 처리완료

    /**
     * @var TradeContract
     */
    private $tradeContract;

    public function __construct(TradeContract $tradeContract)
    {
        $this->tradeContract = $tradeContract;
    }

    public function create($attributes)
    {
        return $this->tradeContract->create($attributes);
    }

    public function all()
    {
        return $this->tradeContract->all();
    }

    public function find($id)
    {
        return $this->tradeContract->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->tradeContract->find($id)->update($attributes);
    }

    public function findLock($id)
    {
        return $this->tradeContract->where('id', $id)->lockForUpdate()->first();
    }

    public function whereLock($condition)
    {
        return $this->tradeContract->where($condition)->lockForUpdate()->get();
    }

    public function where($condition)
    {
        return $this->tradeContract->where($condition);
    }

    public function whereOrderBy($condition, $orderBy, $limit = 10)
    {
        $query = $this->tradeContract->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'desc');
        }

        return $query->limit($limit)->get();
    }

    public function whereOrderByOne($condition, $orderBy)
    {
        $query = $this->tradeContract->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'desc');
        }

        return $query->first();
    }

    public function getList($condition, $orderBy, $offset, $limit)
    {
        $query = "SELECT * FROM tpurcow_trade_contract";
        $where = $this->getListWhere($condition);
        if (!empty($where['whereStr'])) {
            $query .= $where['whereStr'];
        }

        $query .= " ORDER BY ".$orderBy['column']." ".$orderBy['sort'];
        $query .= " LIMIT ".$offset.", ".$limit;

        $list = DB::select($query, $where['whereParam']);
        $total = $this->getListCount($where);

        return compact('list', 'total');
    }

    public function getListCount($where)
    {
        $query = "SELECT COUNT(*) AS cnt FROM tpurcow_trade_contract".$where['whereStr'];
        $result = DB::selectOne($query, $where['whereParam']);

        return $result->cnt;
    }

    public function getListWhere($condition)
    {
        $whereStr = "";
        $where = [];
        $whereParam = [];

        if (!empty($condition['type']) && !empty($condition['member_id'])) {
            if ($condition['type'] == 'buy') {
                $where[] = "buy_member_id = ?";
            } else {
                $where[] = "sell_member_id = ?";
            }
            $whereParam[] = $condition['member_id'];
        } elseif (!empty($condition['member_id'])) {
            $where[] = "(buy_member_id = ? OR sell_member_id = ?)";
            $whereParam[] = $condition['member_id'];
            $whereParam[] = $condition['member_id'];
        }

        if (!empty($condition['target_currency_id'])) {
            $where[] = "target_currency_id = ?";
            $whereParam[] = $condition['target_currency_id'];
        }

        if (!empty($condition['startDate']) && !empty($condition['endDate'])) {
            $where[] = "created_at BETWEEN ? AND ?";
            $whereParam[] = $condition['startDate'];
            $whereParam[] = $condition['endDate'];
        }

        if (!empty($where)) {
            $whereStr = " WHERE ".implode(' AND ', $where);
        }

        return compact('whereStr', 'whereParam');
    }


    public function paginate($perPage, $condition, $orderBy = [])
    {
        $query = DB::table('trade_contract as tc')
            ->leftJoin('member as buyMem', 'tc.buy_member_id', '=', 'buyMem.member_id')
            ->leftJoin('member as sellMem', 'tc.sell_member_id', '=', 'sellMem.member_id')
            ->join('currency_info as targetInfo', 'targetInfo.id', '=', 'tc.target_currency_id')
            ->join('currency_info as baseInfo', 'baseInfo.id', '=', 'tc.base_currency_id')
            ->select('tc.id as trade_id', 'buyMem.member_id as buy_member_id', 'buyMem.name as buy_name', 'buyMem.phone as buy_phone',
                'sellMem.member_id as sell_member_id', 'sellMem.name as sell_name', 'sellMem.phone as sell_phone',
                'targetInfo.symbol as targetSymbol', 'tc.buy_order_id as buy_order_id', 'tc.sell_order_id as sell_order_id',
                'tc.target_currency_id as target_currency_id', 'baseInfo.symbol as baseSymbol', 'tc.buy_fee as buy_fee', 'tc.sell_fee as sell_fee',
                'tc.price as price', 'tc.amount as amount', 'tc.created_at as created_at', 'tc.base_currency_id as base_currency_id')
            ->whereNotNull(['buyMem.member_id', 'sellMem.member_id']);

        if (!empty($condition['name'])) {
            $query->where('buyMem.name', 'like', "%".$condition['name']."%")->orWhere('sellMem.name', 'like', "%".$condition['name']."%");
        }

        if (!empty($condition['member_id'])) {
            $query->where('tc.buy_member_id', $condition['member_id'])->orWhere('tc.sell_member_id', $condition['member_id']);
        }

        if (!empty($condition['phone'])) {
            $query->where('buyMem.phone', 'like', "%".$condition['phone']."%")->orWhere('sellMem.phone', 'like', "%".$condition['phone']."%");
        }

        if (!empty($condition['target_currency_id'])) {
            $query->where('tc.target_currency_id', $condition['target_currency_id']);
        }

        if (!empty($condition['base_currency_id'])) {
            $query->where('tc.base_currency_id', $condition['base_currency_id']);
        }

        if (!empty($condition['order_id'])) {
            $query->where('tc.buy_order_id', $condition['order_id'])->orWhere('tc.sell_order_id', $condition['order_id']);
        }

        if (!empty($condition['trade_id'])) {
            $query->where('tc.id', $condition['trade_id']);
        }

        if (!empty($condition['start_time']) && !empty($condition['end_time'])) {
            $query->where('tc.created_at', '>=', $condition['start_time']);
            $query->where('tc.created_at', '<=', $condition['end_time']);
        }

        if ($orderBy['column'] == null || $orderBy['sort'] == null) {
            $query->orderBy('trade_id', 'desc');
        } else {
            $query->orderBy($orderBy['column'], $orderBy['sort']);
        }


        return $query->paginate($perPage);
    }

    public function getTradeInfo($startTime, $endTime)
    {
        $query = DB::table(DB::raw('tpurcow_trade_contract as tc'))
            ->groupBy(DB::raw("DATE_FORMAT(tc.created_at, '%Y-%m-%d'), target_currency_id, base_currency_id, price"))
            ->whereBetween(DB::raw("tc.created_at"), [$startTime, $endTime])
            ->select(DB::raw("DATE_FORMAT(tc.created_at, '%Y-%m-%d') as date ,sum(buy_fee) as bFee,sum(sell_fee) as sFee, price,
                                    sum(amount) as amount, price*sum(amount) as result, target_currency_id,
                                    base_currency_id,info1.symbol as target_symbol, info2.symbol as base_symbol"))
            ->join(DB::raw('tpurcow_currency_info as info1'), DB::raw('info1.id'), '=', DB::raw('target_currency_id'))
            ->join(DB::raw('tpurcow_currency_info as info2'), DB::raw('info2.id'), '=', DB::raw('base_currency_id'));

        $result = DB::table($query)->groupBy(DB::raw('date,target_currency_id,base_currency_id'))
            ->select(DB::raw('date as created_at,sum(bFee) as buy_fee ,sum(sFee) as sell_fee ,sum(result) as total_result,sum(amount) as amount,
            target_currency_id,base_currency_id, target_symbol, base_symbol'))
            ->orderBy('date', 'asc')
            ->orderBy('target_currency_id', 'asc');

        return $result->get();
    }

}
