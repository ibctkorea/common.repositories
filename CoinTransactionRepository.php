<?php


namespace App\Repositories;


use App\Models\CoinTransaction;
use Illuminate\Support\Facades\DB;

class CoinTransactionRepository
{
    const TYPE_DEPOSIT = 'deposit';
    const TYPE_DEPOSIT_INTERNAL = 'deposit_internal';
    const TYPE_WITHDRAW = 'withdraw';
    const TYPE_WITHDRAW_INTERNAL = 'withdraw_internal';

    // S-성공, I-진행중, C-취소, F-실패
    const STATUS_SUCCESS = 'S';
    const STATUS_ING = 'I';
    const STATUS_WAIT = 'W';
    const STATUS_APPROVE = 'A';
    const STATUS_WAIT_VERIFICATE = 'V';
    const STATUS_CANCEL = 'C';
    const STATUS_FAIL = 'F';
    /**
     * @var CoinTransaction
     */
    private $coinTransaction;

    public function __construct(CoinTransaction $coinTransaction)
    {
        $this->coinTransaction = $coinTransaction;
    }

    public function create($attributes)
    {
        return $this->coinTransaction->create($attributes);
    }

    public function all()
    {
        return $this->coinTransaction->all();
    }

    public function find($id)
    {
        return $this->coinTransaction->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->coinTransaction->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->coinTransaction->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->coinTransaction->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchParams, $attributes)
    {
        return $this->coinTransaction->updateOrCreate($searchParams, $attributes);
    }

    public function where($attr)
    {
        return $this->coinTransaction->where($attr);
    }

    public function whereColumn($column, $oper, $value)
    {
        return $this->coinTransaction->where($column, $oper, $value);
    }

    public function whereIn($column, $values)
    {
        return $this->coinTransaction->whereIn($column, $values);
    }

    public function paginateDeposit($perPage, $condition, $orderBy = [], $type)
    {
        $query = DB::table('coin_transaction')
            ->Join('member', 'coin_transaction.member_id', '=', 'member.member_id')
            ->Join('currency_info', 'coin_transaction.currency_id', '=', 'currency_info.id')
            ->select('coin_transaction.member_id', 'member.name', 'member.email', 'currency_info.symbol',
                'coin_transaction.amount', 'coin_transaction.fee',
                'coin_transaction.receiver', 'coin_transaction.hash as txid',
                'currency_info.address_web', 'currency_info.txid_web',
                'coin_transaction.status as status', 'coin_transaction.updated_at');
        if ($type != null) {
            $query->whereIn('coin_transaction.type', $type);
        }

        if (!empty($condition['email'])) {
            $query->where('member.email', $condition['email']);
        }

        if (!empty($condition['member_id'])) {
            $query->where('coin_transaction.member_id', $condition['member_id']);
        }

        if (!empty($condition['currency_id'])) {
            $query->where('coin_transaction.currency_id', $condition['currency_id']);
        }

        if (!empty($condition['receiver'])) {
            $query->where('coin_transaction.receiver', $condition['receiver']);
        }

        if (!empty($condition['hash'])) {
            $query->where('coin_transaction.hash', $condition['hash']);
        }

        if (!empty($condition['status'])) {
            $query->where('coin_transaction.status', $condition['status']);
        }

        if ($orderBy['column'] == null || $orderBy['sort'] == null) {
            $query->orderBy('updated_at', 'desc');
        } else {
            $query->orderBy($orderBy['column'], $orderBy['sort']);
        }

        return $query->paginate($perPage);
    }

    public function paginateWithdraw($perPage, $condition, $orderBy = [], $type)
    {
        $query = DB::table('coin_transaction')
            ->Join('member', 'coin_transaction.member_id', '=', 'member.member_id')
            ->Join('currency_info', 'coin_transaction.currency_id', '=', 'currency_info.id')
            ->join('currency_user', function ($join) {
                $join->on('coin_transaction.currency_id', '=', 'currency_user.currency_id')
                    ->on('coin_transaction.member_id', '=', 'currency_user.member_id');
            })
            ->select('coin_transaction.id as coin_transaction_id', 'coin_transaction.member_id', 'member.name', 'member.email', 'currency_info.symbol',
                'coin_transaction.amount', 'coin_transaction.fee', 'currency_user.num as assets',
                'coin_transaction.receiver', 'coin_transaction.hash as txid',
                'currency_info.address_web', 'currency_info.txid_web', 'coin_transaction.created_at',
                'coin_transaction.status as status', 'coin_transaction.updated_at', 'coin_transaction.approval_at');
        //$query->where('coin_transaction.currency_id','=','currency_user.currency_id');
        if ($type != null) {
            $query->whereIn('coin_transaction.type', $type);
        }

        if (!empty($condition['email'])) {
            $query->where('member.email', $condition['email']);
        }

        if (!empty($condition['member_id'])) {
            $query->where('coin_transaction.member_id', $condition['member_id']);
        }

        if (!empty($condition['currency_id'])) {
            $query->where('coin_transaction.currency_id', $condition['currency_id']);
        }

        if (!empty($condition['receiver'])) {
            $query->where('coin_transaction.receiver', $condition['receiver']);
        }

        if (!empty($condition['hash'])) {
            $query->where('coin_transaction.hash', $condition['hash']);
        }

        if (!empty($condition['status'])) {
            $query->where('coin_transaction.status', $condition['status']);
        }

        if ($orderBy['column'] == null || $orderBy['sort'] == null) {
            $query->orderBy('updated_at', 'desc');
        } else {
            $query->orderBy($orderBy['column'], $orderBy['sort']);
        }

        return $query->paginate($perPage);
    }


    public function statusList($type)
    {
        $query = DB::table('coin_transaction')->select('status')->distinct();
        if ($type != null) {
            $query->where('type', 'like', $type);
        }
        return $query->get();
    }

    public function getCoinStatistics($startDate,$endDate)
    {
        $query = DB::table("coin_transaction")
            ->select(DB::raw("replace(replace(type,'withdraw_internal','withdraw'),'deposit_internal','deposit') as type ,updated_at,amount,fee,currency_id"))
            ->where("status", "=", CoinTransactionRepository::STATUS_SUCCESS)
            ->whereBetween(DB::raw("updated_at"), [$startDate, $endDate]);
        $result = DB::table($query)
            ->groupBy(DB::raw("DATE_FORMAT(updated_at, '%Y-%m-%d') , currency_id,type"))
            ->select(DB::raw("DATE_FORMAT(updated_at, '%Y-%m-%d') as date , currency_id , sum(amount) as amount ,sum(fee) as fee,type, count(currency_id) as cnt"));
        return $result->get();
    }

    public function updateDaemonTxId($daemonTx)
    {
        return $this->coinTransaction->where('hash', $daemonTx->hash)->update([
            'daemon_tx_id' => $daemonTx->id
        ]);
    }


}
