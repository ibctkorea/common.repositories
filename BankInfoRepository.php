<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 7. 4
 * Time: 오전 9:24
 */

namespace App\Repositories;


use App\Models\BankInfo;
use Illuminate\Support\Facades\DB;


class BankInfoRepository
{

    /**
     * @var BankInfo
     */
    private $bankInfo;

    public function __construct(BankInfo $bankInfo)
    {
        $this->bankInfo = $bankInfo;
    }

    public function create($attributes)
    {
        return $this->bankInfo->create($attributes);
    }

    public function all()
    {
        return $this->bankInfo->all();
    }

    public function find($id)
    {
        return $this->bankInfo->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->bankInfo->find($id)->update($attributes);
    }

    public function findLock($id)
    {
        return $this->bankInfo->where('id', $id)->lockForUpdate()->first();
    }

    public function whereLock($condition)
    {
        return $this->bankInfo->where($condition)->lockForUpdate()->get();
    }

    public function where($condition)
    {
        return $this->bankInfo->where($condition);
    }
    public function orWhere($condition)
    {
        return $this->bankInfo->orWhere($condition);
    }
    public function updateOrCreate($searchParams, $attributes)
    {
        return $this->bankInfo->updateOrCreate($searchParams, $attributes);
    }


}
