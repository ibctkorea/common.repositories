<?php

namespace App\Repositories;


use App\Models\WithdrawList;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class WithdrawListRepository
{
    const WITHDRAW_STATUS_COMPLETE=3;
    /**
     * @var WithdrawList
     */
    private $withdrawList;

    public function __construct(WithdrawList $withdrawList)
    {
        $this->withdrawList = $withdrawList;
    }

    public function create($attributes)
    {
        return $this->withdrawList->create($attributes);
    }

    public function all()
    {
        return $this->withdrawList->all();
    }

    public function find($id)
    {
        return $this->withdrawList->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->withdrawList->find($id)->update($attributes);
    }

    public function findLock($id)
    {
        return $this->withdrawList->where('id', $id)->lockForUpdate()->first();
    }

    public function whereLock($condition)
    {
        return $this->withdrawList->where($condition)->lockForUpdate()->get();
    }

    public function where($condition)
    {
        return $this->withdrawList->where($condition);
    }

    public function whereIn($column, $values)
    {
        return $this->withdrawList->whereIn($column, $values);
    }

    public function orWhere($condition)
    {
        return $this->withdrawList->orWhere($condition);
    }


    public function whereOrderBy($condition, $orderBy, $limit = 10)
    {
        $query = $this->withdrawList->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'desc');
        }

        return $query->limit($limit)->get();
    }

    public function whereOrderByOne($condition, $orderBy)
    {
        $query = $this->withdrawList->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'desc');
        }

        return $query->first();
    }

    public function getWithdrawKrwList($startTime, $endTime)
    {
        $query = DB::table('withdrawlist')->groupBy(DB::raw("FROM_UNIXTIME(editdate,'%Y-%m-%d')"))
            ->select(DB::raw("count(seq) as withdraw_cnt, FROM_UNIXTIME(editdate,'%Y-%m-%d') as date,sum(fees) as fee ,sum(amount) as withdraw_amount"))
            ->where('nstatus', '=', self::WITHDRAW_STATUS_COMPLETE)
            ->whereBetween(DB::raw("editdate"), [$startTime, $endTime]);//Carbon::today()]) //today 오늘 00:00:00 까지
        return $query->get();
    }


}
