<?php

namespace App\Repositories;


use App\Models\Deposit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DepositRepository
{
    /**
     * @var Deposit
     */
    const DEPOSIT_STATUS_COMPLETE = 3;      // 처리완료

    private $deposit;

    public function __construct(Deposit $deposit)
    {
        $this->deposit = $deposit;
    }

    public function create($attributes)
    {
        return $this->deposit->create($attributes);
    }

    public function all()
    {
        return $this->deposit->all();
    }

    public function find($id)
    {
        return $this->deposit->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->deposit->find($id)->update($attributes);
    }

    public function findLock($id)
    {
        return $this->deposit->where('id', $id)->lockForUpdate()->first();
    }

    public function whereLock($condition)
    {
        return $this->deposit->where($condition)->lockForUpdate()->get();
    }

    public function where($condition)
    {
        return $this->deposit->where($condition);
    }

    public function whereIn($column, $values)
    {
        return $this->deposit->whereIn($column, $values);
    }

    public function orWhere($condition)
    {
        return $this->deposit->orWhere($condition);
    }


    public function whereOrderBy($condition, $orderBy, $limit = 10)
    {
        $query = $this->deposit->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'desc');
        }

        return $query->limit($limit)->get();
    }

    public function whereOrderByOne($condition, $orderBy)
    {
        $query = $this->deposit->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'desc');
        }

        return $query->first();
    }

    public function getDepositKrwList($startTime,$endTime)
    {
        $query = DB::table('deposit')->groupBy(DB::raw("FROM_UNIXTIME(editdate,'%Y-%m-%d')"))
            ->select(DB::raw("count(deposit_id) as deposit_cnt, FROM_UNIXTIME(editdate,'%Y-%m-%d') as date ,sum(nAmount2) as deposit_amount"))
            ->where('nstatus', '=', self::DEPOSIT_STATUS_COMPLETE)
            ->whereBetween(DB::raw("editdate"), [$startTime, $endTime]);//Carbon::today()]) //today 오늘 00:00:00 까지
        return $query->get();
    }

}
