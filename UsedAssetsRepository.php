<?php


namespace App\Repositories;



use App\Models\UsedAssets;
use Illuminate\Support\Facades\DB;

class UsedAssetsRepository
{
    const TYPE_DEPOSIT = 'deposit';
    const TYPE_WITHDRAW = 'withdraw';
    /**
     * @var UsedAssets
     */
    private $usedAssets;

    public function __construct(UsedAssets $usedAssets)
    {
        $this->usedAssets = $usedAssets;
    }

    public function create($attributes)
    {
        return $this->usedAssets->create($attributes);
    }

    public function all()
    {
        return $this->usedAssets->all();
    }

    public function find($id)
    {
        return $this->usedAssets->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->usedAssets->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->usedAssets->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->usedAssets->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchParams, $attributes)
    {
        return $this->usedAssets->updateOrCreate($searchParams, $attributes);
    }

    public function where($attr)
    {
        return $this->usedAssets->where($attr);
    }

    public function whereIn($column, $values)
    {
        return $this->usedAssets->whereIn($column, $values);
    }

    public function getUsedAssetByMember($member_id)
    {
        return DB::select("SELECT currency_id, SUM(amount) AS amount FROM tpurcow_used_assets WHERE member_id = ? and deleted_at is null GROUP BY currency_id", [$member_id]);
    }


    public function getTotalUsedAssets()
    {
        $query = DB::table('used_assets')
            ->groupBy('currency_id')
            ->select(DB::raw('currency_id,sum(amount) as total_used_assets'))
            ->where('deleted_at','=',null);
        return $query->get();
    }
}
