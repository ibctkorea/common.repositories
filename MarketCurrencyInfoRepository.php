<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 4:29
 */

namespace App\Repositories;


use App\Models\MarketCurrencyInfo;
use Illuminate\Support\Facades\DB;

class MarketCurrencyInfoRepository
{

    CONST KRW_COIN_ID = 189;
    CONST USDT_COIN_ID = 202;
    CONST ETH_COIN_ID = 187;
    CONST BTC_COIN_ID = 185;


    private $marketCurrencyInfo;

    public function __construct(MarketCurrencyInfo $marketCurrencyInfo)
    {
        $this->marketCurrencyInfo = $marketCurrencyInfo;
    }

    public function create($attributes)
    {
        return $this->marketCurrencyInfo->create($attributes);
    }

    public function all()
    {
        return $this->marketCurrencyInfo->all();
    }

    public function where($condition, $value)
    {
        return $this->marketCurrencyInfo->where($condition, $value);
    }

    public function find($id)
    {
        return $this->marketCurrencyInfo->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->marketCurrencyInfo->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->marketCurrencyInfo->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->marketCurrencyInfo->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchAttributes, $setAttributes)
    {
        return $this->marketCurrencyInfo->updateOrCreate($searchAttributes, $setAttributes);
    }

    public function findBySymbol($symbol)
    {
        return $this->marketCurrencyInfo->where("currency_mark", $symbol)->first();
    }

    public function marketBaseList()
    {
        $result = DB::select("
            SELECT DISTINCT market_info_id 
            from tpurcow_market_currency_info");

        return $result;
    }

    public function getPrecisionId($trade_id, $base_id)
    {
        $data = $this->marketCurrencyInfo->where('currency_info_id', $trade_id)->where('market_info_id', $base_id)->first();
        return $data;
    }

    public function whereArray($attr)
    {
        return $this->marketCurrencyInfo->where($attr);
    }

    public function whereOrderByCoin($condition, $value)
    {
        return $this->marketCurrencyInfo->where($condition, $value)->orderBy(DB::raw("field(market_info_id,187,189) DESC, market_info_id"));
    }
}

