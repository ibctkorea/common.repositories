<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 4:29
 */

namespace App\Repositories;


use App\Models\CurrencyToken;

class CurrencyTokenRepository
{
    private $currencyToken;

    public function __construct(CurrencyToken $currencyToken)
    {
        $this->currencyToken = $currencyToken;
    }

    public function create($attributes)
    {
        return $this->currencyToken->create($attributes);
    }

    public function all()
    {
        return $this->currencyToken->all();
    }

    public function find($id)
    {
        return $this->currencyToken->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->currencyToken->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->currencyToken->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->currencyToken->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchAttributes, $setAttributes)
    {
        return $this->currencyToken->updateOrCreate($searchAttributes, $setAttributes);
    }

    public function where($condition)
    {
        return $this->currencyToken->where($condition);
    }
}