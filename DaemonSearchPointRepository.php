<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 4:29
 */

namespace App\Repositories;


use App\Models\DaemonSearchPoint;

class DaemonSearchPointRepository
{
    private $daemonSearchPoint;

    const TYPE_ADDR_INDEX = 'addr_index';

    public function __construct(DaemonSearchPoint $daemonSearchPoint)
    {
        $this->daemonSearchPoint = $daemonSearchPoint;
    }

    public function create($attributes)
    {
        return $this->daemonSearchPoint->create($attributes);
    }

    public function all()
    {
        return $this->daemonSearchPoint->all();
    }

    public function find($id)
    {
        return $this->daemonSearchPoint->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->daemonSearchPoint->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->daemonSearchPoint->find($id)->delete();
    }

    public function where($attr)
    {
        return $this->daemonSearchPoint->where($attr);
    }

    public function whereIn($column, $values)
    {
        return $this->daemonSearchPoint->whereIn($column, $values);
    }

    public function firstOrCreate($attributes)
    {
        return $this->daemonSearchPoint->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchAttributes, $setAttributes)
    {
        return $this->daemonSearchPoint->updateOrCreate($searchAttributes, $setAttributes);
    }
}