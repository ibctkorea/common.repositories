<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 4:29
 */

namespace App\Repositories;


use App\Models\GenerateAddress;
use Illuminate\Support\Facades\DB;

class GenerateAddressRepository
{
    private $generateAddress;

    const STATUS_WAIT = 'W';
    const STATUS_ING = 'I';
    const STATUS_SUCCESS = 'S';
    const STATUS_FAIL = 'F';

    public function __construct(GenerateAddress $generateAddress)
    {
        $this->generateAddress = $generateAddress;
    }

    public function create($attributes)
    {
        return $this->generateAddress->create($attributes);
    }

    public function insert($attributes)
    {
        return $this->generateAddress->insert($attributes);
    }

    public function all()
    {
        return $this->generateAddress->all();
    }

    public function find($id)
    {
        return $this->generateAddress->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->generateAddress->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->generateAddress->find($id)->delete();
    }

    public function firstOrCreate($attributes)
    {
        return $this->generateAddress->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchParams, $attributes)
    {
        return $this->generateAddress->updateOrCreate($searchParams, $attributes);
    }

    public function where($attr)
    {
        return $this->generateAddress->where($attr);
    }

    public function whereIn($column, $values)
    {
        return $this->generateAddress->whereIn($column, $values);
    }

    public function paginate($perPage, $condition, $orderBy = [])
    {
        $query = DB::table('generate_address')
            //일단 데스트에서 나중에 join으로 바꿔줘도됨 1:1이기떄문에 지금은 test계정을 지워서
            ->leftJoin('member', 'generate_address.member_id', '=', 'member.member_id')
            ->leftJoin('currency_info', 'generate_address.currency_id', '=', 'currency_info.id')
            ->where('generate_address.status', 'S') //address 할당 받은 상태 값
            ->select('generate_address.id', 'generate_address.member_id', 'currency_info.symbol', 'name', 'address', 'email', 'generate_address.updated_at');
        if (!empty($condition['email'])) {
            $query->where('email', $condition['email']);
        }
        if (!empty($condition['member_id'])) {
            $query->where('generate_address.member_id', $condition['member_id']);
        }
        if (!empty($condition['currency_id'])) {
            $query->where('currency_id', $condition['currency_id']);
        }

        if ($orderBy['column'] == null || $orderBy['sort'] == null) {
            $query->orderBy('updated_at', 'desc');
        } else {
            $query->orderBy($orderBy['column'], $orderBy['sort']);
        }

        return $query->paginate($perPage);
    }
}
