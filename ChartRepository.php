<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 7. 4
 * Time: 오전 9:24
 */

namespace App\Repositories;


use App\Models\Chart;

class ChartRepository
{

    private $chart;


    public function __construct(Chart $chart)
    {
        $this->chart = $chart;
    }

    public function find($id)
    {
        return $this->chart->find($id);
    }

    public function where($condition)
    {
        return $this->chart->where($condition);
    }
    public function create($attributes)
    {
        return $this->chart->create($attributes);
    }

    public function whereOrderBy($condition, $orderBy)
    {
        $query = $this->chart->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'asc');
        }
        return $query->get();
    }

    public function whereIn($column, $values)
    {
        return $this->chart->whereIn($column, $values);
    }
    public function all()
    {
        return $this->chart->all();
    }
    public function whereOrderByFirst($condition, $orderBy)
    {
        $query = $this->chart->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'asc');
        }
        return $query->first();
    }

}
