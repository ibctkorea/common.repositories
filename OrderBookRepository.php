<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 7. 4
 * Time: 오전 9:24
 */

namespace App\Repositories;


use App\Models\OrderBook;
use Illuminate\Support\Facades\DB;

class OrderBookRepository
{
    const STATUS_REQ_MATCHING = 0;  // 매칭요청
    const STATUS_WAIT = 1;          // 대기
    const STATUS_REQ_COMPLETE = 2;  // 완료처리요청
    const STATUS_COMPLETE = 3;      // 처리완료
    const STATUS_CANCEL = 4;        // 취소

    /**
     * @var OrderBook
     */
    private $orderBook;

    public function __construct(OrderBook $orderBook)
    {
        $this->orderBook = $orderBook;
    }

    public function create($attributes)
    {
        return $this->orderBook->create($attributes);
    }

    public function all()
    {
        return $this->orderBook->all();
    }

    public function find($id)
    {
        return $this->orderBook->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->orderBook->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->orderBook->find($id)->delete();
    }

    public function findLock($id)
    {
        return $this->orderBook->where('id', $id)->lockForUpdate()->first();
    }

    public function whereLock($condition)
    {
        return $this->orderBook->where($condition)->lockForUpdate()->get();
    }

    public function where($condition)
    {
        return $this->orderBook->where($condition);
    }

    public function whereOrderBy($condition, $orderBy, $limit = 10)
    {
        $query = $this->orderBook->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'desc');
        }

        return $query->limit($limit)->get();
    }

    public function findOrderByLock($condition, $orderBy)
    {
        $query = $this->orderBook->where($condition)->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'desc');
        }

        return $query->lockForUpdate()->first();
    }

    public function whereOrderByLock($condition, $orderBy, $limit)
    {
        $query = $this->orderBook->where($condition);

        $orderByKey = false;
        if (!empty($orderBy)) {
            foreach ($orderBy as $column => $sort) {
                $query->orderBy($column, $sort);
                if ($column == 'id') {
                    $orderByKey = true;
                }
            }
        }

        if (!$orderByKey) {
            $query->orderBy('id', 'desc');
        }

        return $query->limit($limit)->lockForUpdate()->get();
    }

    public function marketPriceList($condition, $orderBy)
    {
        $result = DB::table('order_book')
            ->select('price')
            ->distinct()
            ->whereIn('status', [0, 1])
            ->where('remain_amount', '>', 0)
            ->where('base_currency_id', $condition['base_currency_id'])
            ->where('target_currency_id', $condition['target_currency_id'])
            ->where('type', $condition['type'])
            ->orderBy($orderBy['column'], $orderBy['sort'])
            ->get();
        return $result;
    }

    public function priceData($condition)
    {
        $result = DB::table('order_book')
            ->select('remain_amount')
            ->whereIn('status', [0, 1])
            ->where('remain_amount', '>', 0)
            ->where('base_currency_id', $condition['base_currency_id'])
            ->where('target_currency_id', $condition['target_currency_id'])
            ->where('price', $condition['price'])
            ->where('type', $condition['type'])
            ->get();
        return $result;
    }

    public function getList($condition, $orderBy, $offset, $limit)
    {
        $query = "SELECT * FROM tpurcow_order_book";
        $condition['deleted_at'] = true;
        $where = $this->getListWhere($condition);
        if (!empty($where['whereStr'])) {
            $query .= $where['whereStr'];
        }

        $query .= " ORDER BY ".$orderBy['column']." ".$orderBy['sort'];
        $query .= " LIMIT ".$offset.", ".$limit;

        $list = DB::select($query, $where['whereParam']);
        $total = $this->getListCount($where);

        return compact('list', 'total');
    }

    public function getListCount($where)
    {
        $query = "SELECT COUNT(*) AS cnt FROM tpurcow_order_book".$where['whereStr'];
        $result = DB::selectOne($query, $where['whereParam']);

        return $result->cnt;
    }

    public function getListWhere($condition)
    {
        $whereStr = "";
        $where = [];
        $whereParam = [];
        if (!empty($condition['member_id'])) {
            $where[] = "member_id = ?";
            $whereParam[] = $condition['member_id'];
        }

        if (!empty($condition['target_currency_id'])) {
            $where[] = "target_currency_id = ?";
            $whereParam[] = $condition['target_currency_id'];
        }

        if (!empty($condition['type'])) {
            $where[] = "type = ?";
            $whereParam[] = $condition['type'];
        }

        if (!empty($condition['startDate']) && !empty($condition['endDate'])) {
            $where[] = "created_at BETWEEN ? AND ?";
            $whereParam[] = $condition['startDate'];
            $whereParam[] = $condition['endDate'];
        }

        if (!empty($condition['status']) && $condition['status'] == 'not') {
            $where[] = "status IN (0, 1)";
        }

        if (!empty($condition['deleted_at'])) {
            $where[] = "deleted_at IS NULL";
        }

        if (!empty($where)) {
            $whereStr = " WHERE ".implode(' AND ', $where);
        }

        return compact('whereStr', 'whereParam');
    }

    public function paginate($perPage, $condition, $orderBy = [])
    {
        $query = DB::table('order_book as ob')
            ->leftJoin('member as mem', 'ob.member_id', '=', 'mem.member_id')
            ->join('currency_info as targetInfo', 'targetInfo.id', '=', 'ob.target_currency_id')
            ->join('currency_info as baseInfo', 'baseInfo.id', '=', 'ob.base_currency_id')
            ->select('ob.id as order_id', 'mem.member_id as member_id', 'mem.name as name', 'mem.phone as phone', 'targetInfo.symbol as targetSymbol',
                'ob.target_currency_id as target_currency_id', 'baseInfo.symbol as baseSymbol', 'ob.type as type', 'ob.target_amount as target_amount', 'ob.remain_amount as remain_amount',
                'ob.price as price', 'ob.created_at as created_at', 'ob.updated_at as updated_at', 'ob.status as status', 'ob.base_currency_id as base_currency_id', 'ob.target_currency_id as target_currency_id')
            ->whereNotNull(['mem.member_id']);
        if (!empty($condition['name'])) {
            $query->where('mem.name', 'like', "%".$condition['name']."%");
        }

        if (!empty($condition['member_id'])) {
            $query->where('ob.member_id', $condition['member_id']);
        }

        if (!empty($condition['phone'])) {
            $query->where('mem.phone', 'like', "%".$condition['phone']."%");
        }

        if (!empty($condition['target_currency_id'])) {
            $query->where('ob.target_currency_id', $condition['target_currency_id']);
        }

        if (!empty($condition['base_currency_id'])) {
            $query->where('ob.base_currency_id', $condition['base_currency_id']);
        }

        if (!empty($condition['order_id'])) {
            $query->where('ob.id', $condition['order_id']);
        }

        if (!empty($condition['type'])) {
            $query->where('ob.type', $condition['type']);
        }

        if (!empty($condition['status'])) {
            switch ($condition['status']) {
                case 'success':
                    $query->where('ob.status', 3); //완료
                    break;
                case 'cancel' :
                    $query->where('ob.status', 4); //취소
                    break;
                case 'section':
                    $query->where('ob.status', '<=', 2)->whereColumn('ob.remain_amount', '<', 'ob.target_amount');
                    break;
                default :
                    $query->where('ob.status', '<=', 2)->whereColumn('ob.remain_amount', 'ob.target_amount');
                    break;
            }
        }

        if (!empty($condition['start_time']) && !empty($condition['end_time'])) {

            $query->where('ob.created_at', '>=' , $condition['start_time']);
            $query->where('ob.created_at', '<=' , $condition['end_time']);
        }
        if ($orderBy['column'] == null || $orderBy['sort'] == null) {
            $query->orderBy('order_id', 'desc');
        } else {
            $query->orderBy($orderBy['column'], $orderBy['sort']);
        }

        return $query->paginate($perPage);
    }
}
