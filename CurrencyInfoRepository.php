<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 4:29
 */

namespace App\Repositories;


use App\Models\CurrencyInfo;
use Illuminate\Support\Facades\DB;

class CurrencyInfoRepository
{
    // address 방식 : btc, eth, ...
    const ADDRESS_TYPE_ADDRESS = 'address';
    // account에 memo or : xrp, eos, ...
    const ADDRESS_TYPE_SUB_ADDRESS = 'sub_address';
    const MERGE_INACTIVE = 0;
    const MERGE_ACTIVE = 1;
    private $currencyInfo;

    public function __construct(CurrencyInfo $currencyInfo)
    {
        $this->currencyInfo = $currencyInfo;
    }

    public function create($attributes)
    {
        return $this->currencyInfo->create($attributes);
    }

    public function all()
    {
        return $this->currencyInfo->all();
    }

    public function find($id)
    {
        return $this->currencyInfo->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->currencyInfo->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->currencyInfo->find($id)->delete();
    }

    public function where($attr)
    {
        return $this->currencyInfo->where($attr);
    }
    public function whereIn($column, $values)
    {
        return $this->currencyInfo->whereIn($column, $values);
    }
    public function firstOrCreate($attributes)
    {
        return $this->currencyInfo->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchAttributes, $setAttributes)
    {
        return $this->currencyInfo->updateOrCreate($searchAttributes, $setAttributes);
    }

    public function findBySymbol($symbol)
    {
        return $this->currencyInfo->where("symbol", $symbol)->first();
    }

    public function orderBy($orderBy)
    {
        $query = $this->currencyInfo->orderBy($orderBy['column'], $orderBy['sort']);

        if ($orderBy['column'] != 'id') {
            $query->orderBy('id', 'desc');
        }

        return $query->get();
    }

    public function marketCurrencyInfo($baseCurrencyId, $targetCurrencyId)
    {
        $result = DB::selectOne("
            SELECT a.*, 
                b.fee_buy, b.fee_sell, b.trade_status, b.min_unit_price_pct, b.max_unit_price_pct, b.trade_status, b.listed_price, b.active
            FROM tpurcow_currency_info a
                JOIN tpurcow_market_currency_info b
                    ON a.id = b.currency_info_id
            WHERE b.market_info_id = ? AND a.id = ?
        ", [$baseCurrencyId, $targetCurrencyId]);

        return $result;
    }

    public function marketInfo($baseCurrencyId)
    {
        $result = DB::selectOne("
            SELECT a.*, b.min_trade_amount, b.unit_price
            FROM tpurcow_currency_info a
                JOIN tpurcow_market_info b
                    ON a.id = b.id AND b.active = 1
            WHERE a.id = ?
        ", [$baseCurrencyId]);

        return $result;
    }

    public function listedCurrency()
    {
        return DB::select("SELECT currency_info_id, MAX(market_info_id) AS market_info_id FROM tpurcow_market_currency_info GROUP BY currency_info_id");
    }

    public function getCoinInfo()
    {
        $query = DB::table('currency_info')
            ->select('id', 'symbol')
            ->where('active',1)
            ->orderBy(DB::raw("field(id,189) DESC, id"));
        return $query->get();
    }


    public function getListedCurrencyInfo($attr){
        $query = DB::table('market_currency_info')
            ->leftJoin('currency_info','market_currency_info.currency_info_id','=','currency_info.id')
            ->where($attr);
        return $query->first();

    }
}
