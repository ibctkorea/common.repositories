<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 7. 4
 * Time: 오전 9:24
 */

namespace App\Repositories;


use App\Models\ExaminePwdtrade;
use Illuminate\Support\Facades\DB;


class ExaminePwdtradeRepository
{

//1,4 기존 인증은 두단계로 진행되었으나 현재 운영방식은 한달계로 간소화 현 시스템에서는 1,4를 사용하지 않음

    const EXAMINE_APPROVAL = 3;
    const EXAMINE_REJECT = 2;
    const EXAMINE_NOT = 0;

    /**
     * @var ExaminePwdtrade
     */
    private $examinePwdtrade;

    public function __construct(ExaminePwdtrade $examinePwdtrade)
    {
        $this->examinePwdtrade = $examinePwdtrade;
    }

    public function create($attributes)
    {
        return $this->examinePwdtrade->create($attributes);
    }

    public function all()
    {
        return $this->examinePwdtrade->all();
    }

    public function find($id)
    {
        return $this->examinePwdtrade->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->examinePwdtrade->find($id)->update($attributes);
    }

    public function findLock($id)
    {
        return $this->examinePwdtrade->where('id', $id)->lockForUpdate()->first();
    }

    public function whereLock($condition)
    {
        return $this->examinePwdtrade->where($condition)->lockForUpdate()->get();
    }

    public function where($condition)
    {
        return $this->examinePwdtrade->where($condition);
    }
    public function orWhere($condition)
    {
        return $this->examinePwdtrade->orWhere($condition);
    }
    public function updateOrCreate($searchParams, $attributes)
    {
        return $this->examinePwdtrade->updateOrCreate($searchParams, $attributes);
    }
    public function updateDb($condition, $attributes)
    {
        return DB::table('examine_pwdtrade')->where($condition)->update($attributes);
    }

}
