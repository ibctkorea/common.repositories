<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 9. 27
 * Time: 오전 10:39
 */

namespace App\Repositories;


use App\Models\GenerateAddressTransaction;

class GenerateAddressTransactionRepository
{
    const STATUS_WAIT_VERIFICATE = 'V';
    const STATUS_WAIT = 'W';
    const STATUS_SUCCESS = 'S';
    const STATUS_FAIL = 'F';

    private $generateAddressTransaction;

    public function __construct(GenerateAddressTransaction $generateAddressTransaction)
    {
        $this->generateAddressTransaction = $generateAddressTransaction;
    }

    public function create($attributes)
    {
        return $this->generateAddressTransaction->create($attributes);
    }

    public function all()
    {
        return $this->generateAddressTransaction->all();
    }

    public function find($id)
    {
        return $this->generateAddressTransaction->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->generateAddressTransaction->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->generateAddressTransaction->find($id)->delete();
    }

    public function where($condition)
    {
        return $this->generateAddressTransaction->where($condition);
    }

    public function whereIn($column, $arr)
    {
        return $this->generateAddressTransaction->whereIn($column, $arr);
    }

    public function firstOrCreate($attributes)
    {
        return $this->generateAddressTransaction->firstOrCreate($attributes);
    }

    public function updateOrCreate($searchAttributes, $setAttributes)
    {
        return $this->generateAddressTransaction->updateOrCreate($searchAttributes, $setAttributes);
    }
}